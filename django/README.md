1. `docker-compose run web django-admin.py startproject composeexample .`

    This instructs Compose to run django-admin.py startproject composeexample in a container, using the web service’s image and configuration. Because the web image doesn’t exist yet, Compose builds it from the current directory, as specified by the build: . line in docker-compose.yml.
    
    Once the web service image is built, Compose runs it and executes the django-admin.py startproject command in the container. This command instructs Django to create a set of files and directories representing a Django project.

2. Connect to Database
    In this section, you set up the database connection for Django.
    
    In your project directory, edit the composeexample/settings.py file.
    
    Replace the DATABASES = ... with the following:
    ````
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': 'postgres',
            'HOST': 'db',
            'PORT': 5432,
        }
    }
    ````
    These settings are determined by the postgres Docker image specified in docker-compose.yml.

3. Run the `docker-compose up` command from the top level directory for your project.